import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends OnInit {
  title = 'Shop Test';
  version = '0.9.0';

  text: string = 'no Text';

  ngOnInit() {
  }
  
}
