import { ShopTest1Page } from './app.po';

describe('shop-test1 App', function() {
  let page: ShopTest1Page;

  beforeEach(() => {
    page = new ShopTest1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
